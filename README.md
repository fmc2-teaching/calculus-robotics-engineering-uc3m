# Calculus - Robotics Engineering - UC3M 

This is the repository for the notes based on the lectures of the course [_Calculus_](https://aplicaciones.uc3m.es/cpa/generaFicha?est=381&plan=478&asig=19080&idioma=2) for Robotics Engineering at the UC3M taught by [Florio M. Ciaglia](https://floriomciaglia.wordpress.com/) in the academic year 2022/2023. 
The notes are written using [Obsidian for Linux](https://obsidian.md/) and deployed as a [GitLab page](https://fmc2-teaching.gitlab.io/calculus-robotics-engineering-uc3m/) building upon [Scott Hampton's project](https://gitlab.com/gitlab-org/frontend/playground/obsidian-and-gitlab-pages-demo).


## License

This work is licensed under a [CC BY-SA license](https://creativecommons.org/licenses/by-sa/4.0/).
