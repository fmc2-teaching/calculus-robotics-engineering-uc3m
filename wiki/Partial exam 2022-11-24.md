---
hide:
  - toc
---

1) Show that the following limit does not exist
$$
\lim_{(x,y)\rightarrow (0,0)}\; \frac{x^{2} y}{x^{4} + y^{2}}.
$$

**Hint: checking what happens on the lines of the form $y=mx$ is not enough.**


**Solution**: Let us consider $y=mx^{2}$ so that

$$
\lim_{x\rightarrow 0} \frac{mx^{4}}{x^{4} + m^{2}x^{4}}=\frac{m}{1+ m^{2}},
$$

which means that the limit along parabolae depends on the parabola we consider so that the two-variable limit does not exist.


2) Determine if the chain rule holds for $h=f \circ g$ at the point $(1,1)$ when $f\colon \mathbb{R}^{3}\rightarrow\mathbb{R}$ and $g\colon \mathbb{R}$ are given by

$$
f(x,y,z)=x^2 + y^2 + z^2,\qquad g(u,v)=\left(\begin{matrix} u+ v \\ u- v \\ u^2 + v^2\end{matrix}\right),
$$

respectively.

**Solution:** It suffices to compute the matrices of partial derivatives $Df$ and $Dg$ and then note that all matrix elements are continuous functions so that the hypothesis of the [[5.3 Derivatives and differentiability of multivariable vector functions|chain rule proposition]] hold. 


3) Determine the local and global extrema of the function $f\colon\mathbb{R}^{2}\rightarrow \mathbb{R}$ given by

$$
f(x,y)= x^{2} + y^{2} -2x +y
$$

in the region 

$$
\overline{D}=\left\{(x,y)\in\mathbb{R}^{2}\,|\quad x^2 + y^2 \leq 2 \right\}.
$$

**Solution:** First, we note that $\overline{D}$ is closed and bounded because it is a disk of radius $\sqrt{2}$ centered in $(0,0)$, so that there will be global extrema for $f$ on $\overline{D}$. Next, we look for critical points in the interior $D$ of $\overline{D}$ by looking at points at which the gradient $\nabla f$ either does not exist, or it vanishes. A direct computation shows that

$$
\nabla f(x,y)= (2x-2, 2y +1).
$$

Clearly, $\nabla f$ exists everywhere, and it vanishes only at $(1,-\frac{1}{2})$, which belongs to $D$. To check if $(1,-\frac{1}{2})$ is a local maxima/minima or a saddle point, we compute the Hessian

$$
Hf=\left(\begin{matrix}2 & 0 \\ 0 & 2\end{matrix}\right)
$$

which is a matrix independt from $(x,y)$. According to the [[5.4 Local and global extrema of multivariable scalar functions| second partial derivatives test]], the point $(1,-\frac{1}{2})$ is a local minima at which $f$ is equal to $-\frac{5}{4}$. Then, we set up the [[5.4 Local and global extrema of multivariable scalar functions|Lagrange multiplier problem]] to investigate what happens at the boundary of $\overline{D}$:

$$
\left\{ \begin{matrix} 2x - 2 = 2\lambda x \\ 2y + 1= 2\lambda y \\ x^{2} + y^{2} = 2\end{matrix} \right. \Rightarrow \left\{ \begin{matrix} (1-\lambda)x = 1  \\ (1-\lambda )y =-\frac{1}{2} \\ x^{2} + y^{2} = 2\end{matrix} \right. .
$$

It immediately follows that $\lambda\neq 1$ so that we have

$$
\left\{ \begin{matrix} x =\frac{1}{1-\lambda} \\ y= -\frac{1}{2(1-\lambda)} \\ x^{2} + y^{2} = 2\end{matrix} \right.\Rightarrow \left\{ \begin{matrix} x =\frac{1}{1-\lambda} \\ y= -\frac{1}{2(1-\lambda)} \\ \lambda= 1\pm \sqrt{\frac{5}{4}}\end{matrix} \right. ,
$$

which leads to the points $(\frac{2}{\sqrt{5}}, - \frac{1}{\sqrt{5}})$ and $(-\frac{2}{\sqrt{5}},\frac{1}{\sqrt{5}})$. Direct computation shows that $(\frac{2}{\sqrt{5}}, - \frac{1}{\sqrt{5}})$ is a global minimum while $(-\frac{2}{\sqrt{5}},  \frac{1}{\sqrt{5}})$ is a global maximum.



4)  Compute the following anti-derivative:

$$
\int \frac{1}{1+\sin^{2}(x)}\,\cos(x)\,\mathrm{d}x .
$$

**Solution:** What is the derivative of $\arctan(f(x))$?
