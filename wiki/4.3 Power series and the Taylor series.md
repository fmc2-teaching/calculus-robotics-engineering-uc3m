---
hide:
  - toc
---
# 4.3 Power series

Once we understand that it is possible to make sense of the sum of infinite numbers using numerical series, nothing prevents us in trying to develop similar ideas for the sum of an infinite number of functions. In particular, we will only focus our attention on the so-called **power series**.

>[!note] Definition (Power series)
>A power series is a series whose elements are polynomial functions. Specifically:
>$$
>\sum_{n=0}^{+\infty} \;a_{n}\,(x-a)^{n}
>$$
>where $a\in\mathbb{R}$ and $\{a_{n}\}_{n\in\mathbb{N}}$ is a [[4.1 Numerical sequences|numerical sequence]].

How can we investigate the convergence of this type of series? It turns out we can apply the same methods we apply for [[4.2 Numerical series|numerical series]]. In particular, we use the **root test** to obtain that the power series (absolutely) converges if

$$
\lim_{n\rightarrow \infty}\sqrt[n]{|a_{n}|\,|(x-a)^{n}|}=|x-a|\,\left(\lim_{n\rightarrow + \infty} \sqrt[n]{|a_{n}|}\right) < 1 .
$$

The case where the limit is $1$ must be handled with a case-by-case analysis.

The extended real number $\rho\geq 0$ given by

$$
\frac{1}{\rho}:=\lim_{n\rightarrow +\infty} \sqrt[n]{|a_{n}|}
$$

is called the **convergence radius** of the power series because when the power series (absolutely) converges then $|x-a|< \rho$.

If a power series converges in $I=(a-\rho, a+\rho)$ then we can define the scalar function

$$
f(x)=\sum_{n=0}^{+\infty}a_{n}\,(x-a)^{n}
$$

for every $x\in I$. This scalar function is usually a trascendental function, but it has some very relevant regularity properties.

>[!important] Proposition
>Consider the function $f(x)$ defined through the power series
>$$
>f(x)=\sum_{n=0}^{+\infty}a_{n}\,(x-a)^{n}
>$$
>converging in some open interval $I=(a-\rho,a+\rho)$. This function has then infinitely many continuous derivatives in $I$ given by
>$$
>f^{(k)}(x)=\sum_{n=0}^{+\infty}\frac{\mathrm{d}^{k}}{\mathrm{d}x^{k}}\,\left(a_{n}\,(x-a)^{n}\right).
>$$
>In particular, any such function $f$ is continuous.



## 4.3.1 The Taylor series of a scalar function

A particular type of power series is the so-called **Taylor series** associated with a particular scalar function $f$.

>[!note] Definition (Taylor series)
>Let $f$ be a scalar function having continuous derivatives of any order in an interval centered at the point $a$. The power series
>$$
>\sum_{n=0}^{+\infty}\,a_{n}\;(x-a)^{n}=\sum_{n=0}^{+\infty}\,\frac{f^{(n)}(a)}{n!}\;(x-a)^{n} 
>$$ 
>is called the **Taylor series** of $f$ centered at $a$.

>[!important] Theorem (Taylor's theorem)
>Let $f$ be a scalar function with $(k+1)$ continuous derivatives in an open interval $I$ containing the point $a$. Then there is a function $R_{k}(x)$ such that
>$$
>f(x)=\sum_{n=0}^{k}\frac{f^{(n)}(a)}{n!}\,(x-a)^{n} + R_{n}(x)
>$$
>for all $x\in I$. The previous expansion is called the $k$-th order **Taylor expansion** of $f$ and the function $R_{n}(x)$ is called the **remainder**.

If the **Taylor series** of $f$ converges in $I=(a-\rho,a+\rho)$, can we say to what it converges? Looking at Taylor's theorem, we may be tempted to say that if the Taylor series of $f$ converges then it converges to $f$ itself. However, this is not always true! If $f$ is equal to its Taylor series in some open interval $I=(a-\rho,a+\rho)$ then it is called **analytic** in $I$. The prototypical example of non-analytic function with a convergent Taylor series is the function

$$
f(x)=\left\{\begin{matrix} \mathrm{e}^{-\frac{1}{x}} &\quad x>0 \\ 0 & \quad x\leq 0 .
\end{matrix}\right.
$$

A direct computation shows that the Taylor series of $f$ around $x=0$ is made of terms that are all equal to $0$ so that it always converges to the zero function which is clearly different from $f$.