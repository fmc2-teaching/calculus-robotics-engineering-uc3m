---
hide:
  - toc
---
# 2.4 Exercises

1. Use the $(\epsilon,\delta)$-definition of limit to check the following limits: 

$$
\begin{split} 1) \quad & \lim_{x\rightarrow 1}\frac{x}{x+1}=\frac{1}{2}  \\ & \\
2) \quad &  \lim_{x\rightarrow 0} \frac{x^{2}(x+1)}{2x}=0   
\end{split}
$$


2) Using the [[2.2 Properties of limits|pinching theorem]], prove that:

$$
\begin{split}
1) \quad & \lim_{x\rightarrow 0} \sin(x) =0 \\ & \\
2) \quad & \lim_{x\rightarrow 0} \cos(x) =1  
\end{split}
$$


3) Exploit the results of exercise (2) to prove that $\sin(x)$ and $\cos(x)$ are continuous functions.


4) Exploit the results of exercise (2) and the [[2.2 Properties of limits|pinching theorem]] to prove that:

$$
\begin{split}
1) \quad & \lim_{x\rightarrow 0} \frac{\sin(x)}{x} = 1 \\ & \\
2) \quad & \lim_{x\rightarrow 0} \frac{1-\cos(x)}{x} = 0  
\end{split}
$$


5) Exploit the results of exercise (4) and the proposition on [[2.2 Properties of limits|composition of limits]] to prove that:

$$
\begin{split}
1) \quad & \lim_{x\rightarrow 0} \frac{\sin(ax)}{ax} = 1 \qquad \forall\; 0\neq a\in\mathbb{R}\\ & \\
2) \quad & \lim_{x\rightarrow 0} \frac{1-\cos(ax)}{ax} = 0 \qquad \forall\; 0\neq a\in\mathbb{R}  
\end{split}
$$


6) Determine the following limits:

$$
\begin{split}
1) \quad & \lim_{x\rightarrow 0}\, \frac{\sin(5x)}{\sin(2x)} \\ & \\
2) \quad & \lim_{x\rightarrow c} \,\mathrm{e}^{x}\\& \\
3) \quad & \lim_{x\rightarrow 1} \;\sqrt{\frac{\sin(\mathrm{e}^{x-1})}{\mathrm{e}^{x-1}} + 1} \\& \\
4) \quad & \lim_{x\rightarrow 0}\; \frac{\tan^{2}(x^{2})}{x^{3}} \\ & \\
5) \quad & \lim_{x\rightarrow 0} \frac{\tan(x^{2}) + 2x}{x+ x^{2}} \\ & \\
6)  \quad & \lim_{x\rightarrow 0^{\pm}} \;\frac{1}{x} \\& \\
7)   \quad & \lim_{x\rightarrow \pm\infty} \;\frac{1}{x} \\& \\
8) \quad & \lim_{x\rightarrow +\infty} \; \mathrm{e}^{\frac{1}{x}} \\ & \\
9) \quad & \lim_{x\rightarrow +\infty}\,x^{\alpha} \\ & \\
10)  \quad & \lim_{x\rightarrow +\infty }\;\frac{\cos(x)}{x} + 1  \\ & \\
11)   \quad & \lim_{x\rightarrow-\infty}\;\frac{x^{3} + 2x^{2} -x}{3x - 2x^{2}} \\ & \\
12)   \quad & \lim_{x\rightarrow \pm\infty}\; \sin(x) +2 \\ & \\
13) \quad & \lim_{x\rightarrow +\infty} \mathrm{e}^{2 - 4x - 8x^{2}} \\ & \\
14) \quad & \lim_{x\rightarrow +\infty} x\,\tan\left(\frac{1}{x}\right)
\end{split}
$$


7)  Study the continuity of the following functions classifying the eventual discontinuities:

$$
\begin{split}
1) \quad & f(x)=(x^{2} +3) \mathrm{e^{\cos(x)}} \\ & \\
2) \quad & H(x)=\left\{\begin{matrix} 1 & \mbox{ for } x \geq 0 \\ -1  & \mbox{ for } x< 0 \end{matrix}\right. \\ & \\
3)  \quad & f(x)=\frac{\mathrm{e}^{5x} - \cos(x)}{x^{2} - 8x +12} \\ & \\
4) \quad & f(x) = \sqrt{\ln(x^{2} +1)} \\& \\
5) \quad & f(x)=\left\{ \begin{matrix} \frac{\tan(x)}{\sqrt{x}} & \mbox{ for } x> 0 \\ 0 & \mbox{ for } x=0 \\ \mathrm{e}^{x} & \mbox{ for } x<0 \end{matrix}\right.
\end{split}
$$



 




