---
hide:
  - toc
---

1) Compute the following limit explaining the procedure adopted:
$$
\lim_{x\rightarrow 0}\; \frac{\arctan(x + \pi) - \arctan(\pi)}{x}
$$

**Solution**: It is possible to use [[3.2 Properties of derivatives|l'Hôpital's rule]] to solve the indeterminate form. However, a little thought shows that the limit above is nothing but the derivative of $\arctan(x)$ at $\pi$.


2) Compute the derivative of the scalar function $f(x)$ given by:
$$
f(x)= \sqrt{x^{2} + \sqrt{1-\cos^{2}(x)}}\quad\mbox{ in } \left(0,\frac{\pi}{2}\right).
$$

**Solution**: Recall that $1=\sin^{2}(x) + \cos^{2}(x)$ so that $\sqrt{1-\cos^{2}(x)}=\sqrt{\sin^{2}(x)}=|\sin(x)|$. However, $\sin(x)>0$ when $x\in (0,\frac{\pi}{2})$, so that $\sin(x)=|\sin(x)|$ when $x\in (0,\frac{\pi}{2})$. We thus obtain

$$
f(x)= \sqrt{x^{2} + \sqrt{1-\cos^{2}(x)}}=\sqrt{x^{2}+ \sin(x)}
$$
in $(0,\frac{\pi}{2})$. Now, we note that $f(x)=h\circ g(x)$ with $h(x)=\sqrt{x}$ and $g(x)=x^{2} + \sin(x)$, and we apply [[3.2 Properties of derivatives|rule for the derivative of composite functions]] to get

$$
f'(x)=\frac{2x + \cos(x)}{2\sqrt{x^{2} + \sin(x)}}
$$

in $(0,\frac{\pi}{2})$.


3) Determine whether the following series converges explaining which method you use to check convergence:

$$  \sum_{n=0}^{+\infty}\,a_{n}= \sum_{n=0}^{+\infty}\,\frac{4n +7\pi}{3^{n}} .
$$

**Solution**: Applying the [[4.2 Numerical series|ratio test]] we obtain:

$$
\lim_{n\rightarrow \infty} \frac{a_{n+1}}{a_{n}}=\lim_{n\rightarrow\infty} \frac{(4(n+1) + 7\pi)3^{n}}{3^{n+1}(4n +7\pi)}=\lim_{n\rightarrow\infty}\frac{4(n+1) + 7\pi}{3(4n+7\pi)}=\lim_{n\rightarrow\infty} \frac{n(4 + \frac{4+7\pi}{n})}{3n(4+\frac{7\pi}{n})}=\frac{1}{3} ,
$$

which implies that the series converges because the limit is non-negative and smaller than 1.


4) Determine the sum of the following series explaining which method you use to check convergence:

$$
\sum_{n=1}^{+\infty}\,a_{n}= \sum_{n=1}^{+\infty}\,\frac{\sqrt{n+1} - \sqrt{n}}{\sqrt{n(n+1)}}.
$$

**Solution**: If we expand $a_{n}$ we obtain

$$
a_{n}=\frac{1}{\sqrt{n}} - \frac{1}{\sqrt{n+1}}\equiv b_{n} - b_{n+1}.
$$

Therefore, the series is a [telescoping series](https://en.wikipedia.org/wiki/Telescoping_series) and its sum is

$$
\sum_{n=1}^{+\infty}\,a_{n}= b_{1} - \lim_{k\rightarrow +\infty} b_{k+1} = 1.
$$


5) Prove that the derivative of $f(x)=x^{\alpha}$ with $x>0$ and $\alpha\in\mathbb{R}$ is $f'(x)=\alpha x^{\alpha-1}$.

**Solution**:  Note that $x=\mathrm{e}^{\ln(x)}$ because $x>0$. Then, we note that $x^{\alpha}=f\circ g(x)$ with $f(x)=\mathrm{e}^{x}$ and $g(x)=\alpha\ln(x)$, and we use the [[3.2 Properties of derivatives|rule for the derivative of composite functions]] to get

$$
\frac{\mathrm{d}}{\mathrm{d}x} x^{\alpha} = \frac{\mathrm{d}}{\mathrm{d}x} \left(\mathrm{e}^{\alpha\ln(x)}\right)= \mathrm{e}^{\alpha\ln(x)} \frac{\alpha}{x}=\alpha\, x^{\alpha-1}
$$

as requested.


6) Given the expression
$$
f(x)= \mathrm{e}^{-9x^{2} +6x - 4} 
$$
determine:

- its (maximal) domain $\mathrm{Dom}(f)$;
- the points at which the function determined by the rule $f(x)$ and the domain $\mathrm{Dom}(f)$  is continuous and those points at which it is not continuous;
-  all local maxima/minima and all global maxima/minima (if any).

**Solution**: We start noting that $f(x)=h\circ g(x)$ with $h(x)=\mathrm{e}^{x}$ and $g(x)=-9x^{2} +6x - 4$. Both $h(x)$ and $g(x)$ are defined on the whole real line $\mathbb{R}$ so that $f(x)$ too is defined on the whole real line. Moreover, both $h(x)$ and $g(x)$ are continuous on the whole real line so that $f(x)$ is continuous because it's the [[2.3 Continuity|composition of continuous functions]]. Moreover, we can apply the [[3.2 Properties of derivatives||rule for the derivative of composite functions]] to get

$$
f'(x) = -6(3x - 1)\,\mathrm{e}^{-9x^{2} +6x - 4},
$$

which is defined for every $x\in\mathbb{R}=\mathrm{Dom}(f)$ so that $f$ is continous for every $x\in\mathrm{Dom}(f)$ [[3.2 Properties of derivatives|because it is differentiable]] for every $x\in\mathrm{Dom}(f)$. 
There are no boundary points in $\mathrm{Dom}(f)=\mathbb{R}$, so that we only focus on critical points, which are [[3.3 Local and global extrema|determined by the condition]]

$$
f'(x)=0\,\Longleftrightarrow\,-6(3x - 1)\,\mathrm{e}^{-9x^{2} +6x - 4}=0\,\Longleftrightarrow\,x= \frac{1}{3}
$$
since $\mathrm{e}^{-9x^{2} +6x - 4}>0$ for all $x\in\mathbb{R}$. The critical point is a [[3.3 Local and global extrema|local maxima]] since $f'(x)>0$ right before $x=\frac{1}{3}$ and $f'(x)<0$ right after $x=\frac{1}{3}$. 
To check if it is a global maxima we compute

$$
\lim_{x\rightarrow +\infty}f(x)=\lim_{x\rightarrow - \infty}f(x)=0,
$$

and we conclude that $x=\frac{1}{3}$ is a global maxima.
